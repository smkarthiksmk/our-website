import { Component, OnInit, HostListener, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { trigger, state, style, animate, transition } from '@angular/animations';
declare var $: any;
@Component({
  selector: 'app-main-body',
  templateUrl: './main-body.component.html',
  styleUrls: ['./main-body.component.scss'],
  animations: [
    trigger('animationState', [
      state('header-not-scrolled', style({
        position: 'relative',
      })),
      state('header-scrolled', style({
        position: 'fixed',
        width: '100%',
        top: 0,
        zIndex: '10'
      })),
      state('nav-menu-one', style({
        left: '0%'
      })),
      state('nav-menu-two', style({
        left: '20%'
      })),
      state('nav-menu-three', style({
        left: '40%'
      })),
      state('nav-menu-four', style({
        left: '60%'
      })),
      state('nav-menu-five', style({
        left: '80%'
      })),
      state('view-visible', style({
        opacity: 1,
        transform: 'scale(1,1)'
      })),
      state('center-view-visible', style({
        opacity: 1,
        transform: 'scale(1.1,1.1)'
      })),
      state('opportunities-container-vertical-expand', style({
        transform: 'scale(1)',
      })),
      state('opportunities-container-horizontal-expand', style({
        transform: 'scale(1)',
        width: '45%'
      })),
      state('opportunities-content-one-hide', style({
        top: '-100%'
      })),
      state('opportunities-content-two-show', style({
        top: '50%',
        transform: 'translateY(-50%)'
      })),
      state('social-feed-container-visible', style({
        opacity: 1
      })),

      transition('header-not-scrolled <=> header-scrolled', animate('0ms ease-out')),
      transition('* => view-visible', animate('200ms ease')),
      transition('* => nav-menu-one', animate('200ms ease-out')),
      transition('* => nav-menu-two', animate('200ms ease-out')),
      transition('* => nav-menu-three', animate('200ms ease-out')),
      transition('* => nav-menu-four', animate('200ms ease-out')),
      transition('* => nav-menu-five', animate('200ms ease-out')),
      transition('* => opportunities-container-vertical-expand', animate('700ms ease')),
      transition('* => opportunities-container-horizontal-expand', animate('1000ms ease')),
      transition('* => opportunities-content-one-hide', animate('1000ms ease')),
      transition('* => opportunities-content-two-show', animate('1000ms ease')),
      transition('* => social-feed-container-visible', animate('1000ms cubic-bezier(0.47, 0, 0.745, 0.715)')),

    ])
  ]
})
export class MainBodyComponent implements OnInit {
  @ViewChild('header') header: ElementRef;
  @ViewChild('logo') logo: ElementRef;
  @ViewChild('navigationContainer') navigationContainer: ElementRef;
  @ViewChild('bodyContainer') bodyContainer: ElementRef;
  @ViewChild('productContainerRow') productContainerRow: ElementRef;
  @ViewChild('launchedProductsContainer') launchedProductsContainer: ElementRef;
  @ViewChild('opportunitiesContainer') opportunitiesContainer: ElementRef;
  navMenuState: any;
  productContainerOneViewState: any;
  productContainerTwoViewState: any;
  productContainerThreeViewState: any;
  currentFocusedView = 0;
  previousFocusedView = 0;
  navMenuIdentifierState: any;
  opportunitiesDescriptionContentState: any;
  opportunitiesContentOneState: any;
  opportunitiesContentTwoState: any;
  socialFeedContainerState: any;
  constructor() { }

  ngOnInit() {
    this.pageUIAlignments();
  }
  @HostListener('window:scroll', ['$event'])
  scrollHandler(event) {
    this.pageUIAlignments();
  }
  isScrolledIntoView(el) {
    var rect = el.getBoundingClientRect();
    var elemTop = rect.top;
    var elemBottom = rect.bottom;
    var isVisible = elemTop < window.innerHeight && elemBottom >= 0;
    return isVisible;
  }
  scroll(el) {
    el.scrollIntoView();
  }
  pageUIAlignments() {
    // to avoid freeze in the UI in body-container since nav bar becomes sticky
    if (window.pageYOffset > this.header.nativeElement.offsetHeight) {
      this.navMenuState = 'header-scrolled';
      this.logo.nativeElement.style.opacity = 1 + (window.pageYOffset / 300);
      $(".dummy-nav-placeholder").height($(".navigation-container").outerHeight());
    }
    else {
      this.navMenuState = 'header-not-scrolled';
      this.logo.nativeElement.style.opacity = 1 - (window.pageYOffset / 300);
      $(".dummy-nav-placeholder").height(0);
    }

    //to set current nav-menu identifier
    if (this.isScrolledIntoView(this.header.nativeElement)) {
      this.navMenuIdentifierState = 'nav-menu-one';
    }
    else if (this.isScrolledIntoView(this.launchedProductsContainer.nativeElement)) {
      this.navMenuIdentifierState = 'nav-menu-two';
    }
    else if (this.isScrolledIntoView(this.opportunitiesContainer.nativeElement)) {
      this.navMenuIdentifierState = 'nav-menu-four';
    }

    // to show launched products section only when it comes to viewport
    if (this.isScrolledIntoView(this.productContainerRow.nativeElement) && (parseInt($(".product-one").css("opacity"))) == 0) {
      setTimeout(() => {
        this.productContainerTwoViewState = 'center-view-visible'
        setTimeout(() => {
          this.productContainerOneViewState = 'view-visible'
          setTimeout(() => {
            this.productContainerThreeViewState = 'view-visible'
          }, 100);
        }, 100);
      }, 0);
    }
    console.log(parseInt($(".description-content").css("width")));

    if (this.isScrolledIntoView(this.opportunitiesContainer.nativeElement) && (parseInt($(".description-content").css("width"))) == 0) {
      this.opportunitiesDescriptionContentState = 'opportunities-container-vertical-expand';
      let s = this;
      setTimeout(() => {
        s.opportunitiesDescriptionContentState = 'opportunities-container-horizontal-expand';
        s.socialFeedContainerState = "social-feed-container-visible";
        setTimeout(() => {
          s.opportunitiesContentOneState = 'opportunities-content-one-hide';
          s.opportunitiesContentTwoState = 'opportunities-content-two-show';
          
          // setTimeout(() => {
          //   s.socialFeedContainerState = "social-feed-container-visible";
         // }, 100);
        }, 3000);
      }, 800);
    }
  }
}
